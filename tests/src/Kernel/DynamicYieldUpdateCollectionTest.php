<?php

declare(strict_types=1);

namespace Drupal\Tests\dynamic_yield\Kernel;

use Drupal\Core\Database\Connection;
use Drupal\dynamic_yield\DynamicYieldUpdateItem;
use Drupal\dynamic_yield\FeedApiActions;
use Drupal\dynamic_yield\Services\DynamicYieldUpdateCollection;
use Drupal\KernelTests\KernelTestBase;

/**
 * Verifies the operation of DynamicYieldUpdateCollection.
 *
 * @coversDefaultClass \Drupal\dynamic_yield\Services\DynamicYieldUpdateCollection
 * @group dynamic_yield
 */
final class DynamicYieldUpdateCollectionTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dynamic_yield', 'key'];

  /**
   * Injected connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Array of test data.
   *
   * @var \Drupal\dynamic_yield\DynamicYieldUpdateItem[]
   */
  protected array $collectionData;

  /**
   * The class under test.
   *
   * @var \Drupal\dynamic_yield\Services\DynamicYieldUpdateCollection
   */
  protected DynamicYieldUpdateCollection $collection;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('dynamic_yield', [DynamicYieldUpdateCollection::DATA_TABLE_NAME]);
    $this->database = $this->container->get('database');
    $this->collection = new DynamicYieldUpdateCollection($this->database);
    $this->collectionData = [];
    $this->collectionData[] = new DynamicYieldUpdateItem('uuid-2', 'node', 2, FeedApiActions::Update);
    $this->collectionData[] = new DynamicYieldUpdateItem('uuid-3', 'node', 3, FeedApiActions::Update);
    $this->collectionData[] = new DynamicYieldUpdateItem('uuid-1', 'node', 1, FeedApiActions::Update);
  }

  /**
   * Test adding items.
   *
   * @covers ::add
   */
  public function testAdd(): void {
    foreach ($this->collectionData as $item) {
      $this->collection->add($item);
    }
    // Verify 3 items are now stored in the database.
    $count = $this->database->select(DynamicYieldUpdateCollection::DATA_TABLE_NAME, 'dy')
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(3, $count);
    // Test adding a duplicate uuid.
    $matchesExistingItem = new DynamicYieldUpdateItem('uuid-1', 'node', 4, FeedApiActions::Update);
    $this->collection->add($matchesExistingItem);
    // Verify that there are still 3 items in the table.
    $count = $this->database->select(DynamicYieldUpdateCollection::DATA_TABLE_NAME, 'dy')
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(3, $count);
    // Verify that the priority (timestamp) of uuid-1 is unchanged.
    $itemOne = $this->database->select(DynamicYieldUpdateCollection::DATA_TABLE_NAME, 'dy')
      ->fields('dy')
      ->condition('uuid', 'uuid-1')
      ->execute()
      ->fetchAllAssoc('uuid');
    $this->assertEquals(1, $itemOne['uuid-1']->timestamp);
    // Test a delete item.
    $deleteItem = new DynamicYieldUpdateItem('uuid-4', 'node', 4, FeedApiActions::Delete);
    $this->collection->add($deleteItem);
    // Verify 4 items are now stored in the database.
    $count = $this->database->select(DynamicYieldUpdateCollection::DATA_TABLE_NAME, 'dy')
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(4, $count);
  }

  /**
   * Test removing items.
   *
   * @covers ::remove
   */
  public function testRemove(): void {
    foreach ($this->collectionData as $item) {
      $this->collection->add($item);
    }
    array_pop($this->collectionData);
    $this->collection->remove($this->collectionData);
    // Verify that one item remains in the table.
    $count = $this->database->select(DynamicYieldUpdateCollection::DATA_TABLE_NAME, 'dy')
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(1, $count);
    // Verify that uuid-1 remains.
    $itemOne = $this->database->select(DynamicYieldUpdateCollection::DATA_TABLE_NAME, 'dy')
      ->fields('dy')
      ->condition('uuid', 'uuid-1')
      ->execute()
      ->fetchAllAssoc('uuid');
    $this->assertEquals('uuid-1', $itemOne['uuid-1']->uuid);
    // Verify that an error is not thrown if an item NOT in the collection
    // is removed.
    $nonExistingItem = new DynamicYieldUpdateItem('uuid-4', 'node', 4, FeedApiActions::Update);
    $this->collection->remove([$nonExistingItem]);
    // Verify that one item remains in the table.
    $count = $this->database->select(DynamicYieldUpdateCollection::DATA_TABLE_NAME, 'dy')
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(1, $count);
  }

  /**
   * Test getting items by priority.
   *
   * @covers ::get
   */
  public function testGet(): void {
    foreach ($this->collectionData as $item) {
      $this->collection->add($item);
    }
    // Get the two highest priority items, which should be uuid-1 & uuid-2.
    $items = $this->collection->get(2);
    $this->assertCount(2, $items);
    $item = array_shift($items);
    $this->assertInstanceOf(DynamicYieldUpdateItem::class, $item);
    $this->assertEquals('uuid-1', $item->uuid);
    $item = array_shift($items);
    $this->assertInstanceOf(DynamicYieldUpdateItem::class, $item);
    $this->assertEquals('uuid-2', $item->uuid);
    // Verify that one item remains in the table.
    $count = $this->database->select(DynamicYieldUpdateCollection::DATA_TABLE_NAME, 'dy')
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(1, $count);
  }

  /**
   * Counting items.
   *
   * @covers ::count
   */
  public function testCount(): void {
    foreach ($this->collectionData as $item) {
      $this->collection->add($item);
    }
    $this->assertEquals(3, $this->collection->count());
  }

  /**
   * Verifies the check for empty priority queue.
   *
   * @covers ::isEmpty
   */
  public function testIsEmpty(): void {
    // The table starts empty.
    $this->assertTrue($this->collection->isEmpty());
  }

}
