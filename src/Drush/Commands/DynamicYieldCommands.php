<?php

namespace Drupal\dynamic_yield\Drush\Commands;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\dynamic_yield\DynamicYieldBatchQueueUpdates;
use Drupal\dynamic_yield\DynamicYieldBatchSendUpdates;
use Drupal\dynamic_yield\Services\DynamicYieldProductFeedInterface;
use Drupal\dynamic_yield\Services\DynamicYieldUpdateCollectionInterface;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file for dynamic_yield.
 */
final class DynamicYieldCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * Constructs a DynamicYieldCommands object.
   */
  final public function __construct(
    private readonly DynamicYieldProductFeedInterface $dynamicYieldProductFeed,
    private readonly DynamicYieldUpdateCollectionInterface $dynamicYieldUpdates,
    private readonly Connection $database,
    private readonly ConfigFactoryInterface $configFactory,
  ) {
    parent::__construct();
  }

  /**
   * Push updates in a single transaction.
   */
  #[CLI\Command(name: 'dynamic-yield:push', aliases: ['dy-p'])]
  #[CLI\Argument(name: 'count', description: 'The number of items to push.')]
  #[CLI\Usage(name: 'dynamic-yield:push 10', description: 'Push a limited set of updates to Dynamic Yield.')]
  public function singleUpdate(int $count):void {
    if ($this->dynamicYieldUpdates->isEmpty()) {
      $this->io()->info(dt('Dynamic Yield update queue is empty.'));
      return;
    }
    $transaction = $this->dynamicYieldProductFeed->process($count);
    if ($transaction->skipped()) {
      $this->io()->info(dt('Dynamic Yield product feed did not return a transaction'));
      return;
    }
    $this->dynamicYieldProductFeed->verify($transaction);
    if ($transaction->countUpdateItems() === 0) {
      $this->logger()->success(dt('Dynamic Yield api reports all items updated.'));
    }
    else {
      $this->io->caution(dt(
        'Returning {count} items to the queue.',
        ['count' => $transaction->countUpdateItems()],
      ));
      // Place any un-processed items back in the queue.
      foreach ($transaction->getUpdateItems() as $item) {
        $this->dynamicYieldUpdates->add($item);
      }
    }
  }

  /**
   * Push all updates in a batch process.
   */
  #[CLI\Command(name: 'dynamic-yield:push-all', aliases: ['dy-up'])]
  #[CLI\Usage(name: 'dynamic-yield:push-all', description: 'Send all pending updates to Dynamic Yield.')]
  public function updateAll():void {
    $batch = new BatchBuilder();
    $batch->setTitle(dt('Updating feed'))
      ->setFinishCallback([DynamicYieldBatchSendUpdates::class, 'finish'])
      ->setInitMessage(dt('Setting up...'))
      ->setProgressMessage(dt('Processing...'))
      ->setErrorMessage(dt('An error occurred during processing.'))
      ->addOperation([DynamicYieldBatchSendUpdates::class, 'process']);

    batch_set($batch->toArray());

    drush_backend_batch_process();

    $this->io()->text(dt('Dynamic Yield feed update process is complete.'));
  }

  /**
   * Add content to the update queue.
   */
  #[CLI\Command(name: 'dynamic-yield:queue', aliases: ['dy-q'])]
  #[CLI\Usage(name: 'dynamic-yield:queue', description: 'Add content to the update queue.')]
  public function queue():void {
    $settings = $this->configFactory->get('dynamic_yield.settings')->get('content_types');
    $enabled = array_filter($settings, function ($setting) {
      return $setting;
    });
    $bundles = array_keys($enabled);
    $bundlesAll = $bundles;
    array_unshift($bundlesAll, 'all');
    // Duplicate values into keys.
    $choices = array_combine($bundlesAll, $bundlesAll);
    $selection = $this->io()->choice(dt('Choose which node bundles to update'), $choices, 'all');
    if ($selection === 'all') {
      $selection = $bundles;
    }
    else {
      $selection = [$selection];
    }

    $batch = new BatchBuilder();
    $batch->setTitle(dt('Updating feed'))
      ->setFinishCallback([DynamicYieldBatchQueueUpdates::class, 'finish'])
      ->setInitMessage(dt('Setting up...'))
      ->setProgressMessage(dt('Processing...'))
      ->setErrorMessage(dt('An error occurred during processing.'));

    $batch->addOperation([DynamicYieldBatchQueueUpdates::class, 'process'], [$selection]);

    batch_set($batch->toArray());

    drush_backend_batch_process();

    $this->io()->text(dt('Requested content is processed into the Dynamic Yield update queue.'));
  }

  /**
   * Gets the size of the update queue.
   */
  #[CLI\Command(name: 'dynamic-yield:status', aliases: ['dy-s'])]
  #[CLI\Usage(name: 'dynamic-yield:status', description: 'Get the size of the update queue.')]
  public function queueStatus():void {
    $this->io()->info(dt('Dynamic Yield update queue has @count items waiting to be sent.', ['@count' => $this->dynamicYieldUpdates->count()]));
  }

}
