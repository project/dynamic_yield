<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Attribute;

use Drupal\Component\Plugin\Attribute\AttributeBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Identifies dynamic_yield_column plugin implementation classes.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class DynamicYieldColumn extends AttributeBase {

  /**
   * Constructs a new DynamicYieldColumn instance.
   *
   * @param string $id
   *   The plugin ID.
   * @param string $name
   *   The Dynamic Yield column name.
   * @param string|null $sourceField
   *   (optional) A string to provide source information to the plugin.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A brief description of the plugin.
   */
  public function __construct(
    public readonly string $id,
    public readonly string $name,
    public readonly ?string $sourceField = NULL,
    public readonly ?TranslatableMarkup $description = NULL,
  ) {}

}
