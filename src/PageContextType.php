<?php

namespace Drupal\dynamic_yield;

/**
 * Provides allowed values for Dynamic Yield page context.
 *
 * @see https://dy.dev/docs/code-context
 */
enum PageContextType: string {
  case Product = 'PRODUCT';
  case Home = 'HOMEPAGE';
  case Other = 'OTHER';
}
