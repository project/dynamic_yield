<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns a response for our JS cookie tending callback.
 */
final class DynamicYieldCookieXhr extends ControllerBase {

  /**
   * Builds the response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Our minimal response to carry the cookie.
   */
  public function reply(Request $request): JsonResponse {
    $response = new JsonResponse(['success' => TRUE]);
    // We do not want this response cached.  That is declared here and further
    // insured by using a unique random query parameter in
    // dynamic-yield-dyserver.js.
    $response->setCache(['no_store' => TRUE]);
    return $response;
  }

}
