<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dynamic_yield\Services\DynamicYieldProductFeed;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Dynamic Yield routes.
 */
final class DynamicYieldDataTab extends ControllerBase {

  /**
   * The controller constructor.
   */
  private DynamicYieldProductFeed $dynamicYieldFeed;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = new static();
    $instance->dynamicYieldFeed = $container->get('dynamic_yield.product_feed');
    return $instance;
  }

  /**
   * Builds the response.
   *
   * @return mixed[]
   *   The render array.
   */
  public function buildDataTable(NodeInterface $node): array {

    $rows = [];
    $json = $this->dynamicYieldFeed->getEntityData($node);

    foreach ($json as $name => $value) {
      $value = is_bool($value) ? ($value ? 'true' : 'false') : $value;
      $rows[] = [$name, $value];
    }

    $build['data'] = [
      '#type' => 'table',
      '#header' => [$this->t('Column name'), $this->t('Data')],
      '#rows' => $rows,
      '#empty' => $this->t('No columns are defined.'),
      '#sticky' => TRUE,
    ];
    // All urls are displayed here as plain text.
    $jsonFormatted = json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    $build['json'] = [
      '#type' => 'details',
      '#title' => $this->t('JSON'),
      '#open' => FALSE,
      'data' => [
        '#type' => 'markup',
        '#markup' => nl2br($jsonFormatted),
      ],
    ];

    return $build;
  }

  /**
   * Use the node's title.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node.
   *
   * @return string
   *   The node's title.
   */
  public function getTitle(NodeInterface $node): string {
    return $node->label();
  }

}
