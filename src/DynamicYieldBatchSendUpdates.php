<?php

namespace Drupal\dynamic_yield;

/**
 * Provides batch callbacks for sending updates to Dynamic Yield.
 */
class DynamicYieldBatchSendUpdates {

  /**
   * Batch operation callback.
   *
   * @param array $context
   *   Persistent data.
   */
  public static function process(array &$context): void {
    // Get services.
    /** @var \Drupal\dynamic_yield\Services\DynamicYieldProductFeedInterface $productFeed */
    $productFeed = \Drupal::service('dynamic_yield.product_feed');
    /** @var \Drupal\dynamic_yield\Services\DynamicYieldUpdateCollectionInterface $priorityQueue */
    $priorityQueue = \Drupal::service('dynamic_yield.updates');
    // Initiate data on the first step.
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = $priorityQueue->count();
    }
    // Send upto 100 items.
    $transaction = $productFeed->process(100);
    // Update $context.
    $context['results']['transactions'][] = $transaction;
    $remaining = $priorityQueue->count();
    if ($remaining === 0) {
      $context['message'] = t('Sent an update with @transaction items', [
        '@transaction' => $transaction->countUpdateItems(),
      ]);
      $context['finished'] = 1;
    }
    else {
      $context['message'] = t('Sent an update with @transaction items. @remaining items remain.', [
        '@transaction' => $transaction->countUpdateItems(),
        '@remaining' => $remaining,
      ]);
      $context['sandbox']['progress'] += $transaction->countUpdateItems();
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Batch finish callback.
   *
   * @param bool $success
   *   Did the batch succeed in all operations.
   * @param array $results
   *   Result data.
   * @param array $operations
   *   Any remaining operations.
   */
  public static function finish(bool $success, array $results, array $operations): void {
    // Initialize.
    $updated = 0;
    $returned = 0;
    /** @var \Drupal\dynamic_yield\Services\DynamicYieldProductFeedInterface $productFeed */
    $productFeed = \Drupal::service('dynamic_yield.product_feed');
    /** @var \Drupal\dynamic_yield\Services\DynamicYieldUpdateCollectionInterface $priorityQueue */
    $priorityQueue = \Drupal::service('dynamic_yield.updates');
    /** @var \Drupal\dynamic_yield\DynamicYieldUpdateTransaction[] $transactions */
    $transactions = $results['transactions'];
    foreach ($transactions as $transaction) {
      if ($transaction->skipped()) {
        continue;
      }
      $contains = $transaction->countUpdateItems();
      $productFeed->verify($transaction);
      $remaining = $transaction->countUpdateItems();
      $updated += $contains - $remaining;
      // Place any un-processed items back in the queue.
      foreach ($transaction->getUpdateItems() as $item) {
        $priorityQueue->add($item);
      }
      $returned += $remaining;
    }
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addStatus(t('The feed successfully pushed @updated items.', [
        '@updated' => $updated,
      ]));
      if ($returned > 0) {
        $messenger->addWarning(t('@returned items did not verify and were returned to the queue', [
          '@returned' => $returned,
        ]));
      }

    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      $messenger->addError($message);
    }
  }

}
