<?php

namespace Drupal\dynamic_yield;

/**
 * Provides batch queue callbacks for Dynamic Yield.
 */
class DynamicYieldBatchQueueUpdates {

  /**
   * Batch operation callback.
   *
   * @param array $bundles
   *   An array of bundle ids whose members should be added to the queue.
   * @param array $context
   *   Persistent data.
   */
  public static function process(array $bundles, array &$context): void {
    /** @var \Drupal\dynamic_yield\Services\DynamicYieldUpdateCollectionInterface $priorityQueue */
    $priorityQueue = \Drupal::service('dynamic_yield.updates');
    /** @var \Drupal\dynamic_yield\Services\UpdateFactoryInterface $updateFactory */
    $updateFactory = \Drupal::service('dynamic_yield.update.factory');
    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    $database = \Drupal::database();
    // Initiate data on the first step.
    if (!isset($context['sandbox']['progress'])) {
      // Get the item count from the database.
      $itemCount = (int) $database->select('node', 'n')
        ->fields('n', ['uuid'])
        ->condition('type', $bundles, 'IN')
        ->countQuery()
        ->execute()
        ->fetchField();
      if ($itemCount === 0) {
        $context['message'] = t('No content found for the selected bundles');
        $context['finished'] = 1;
        return;
      }
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = $itemCount;
    }

    // Get the ids from the database.
    $items = $database->select('node', 'n')
      ->fields('n', ['nid'])
      ->condition('type', $bundles, 'IN')
      ->range($context['sandbox']['progress'], 200)
      ->execute()
      ->fetchCol();
    // Load the nodes.
    $nodes = $nodeStorage->loadMultiple($items);
    $updates = $updateFactory->filterMultiple($nodes);

    foreach ($updates as $update) {
      $priorityQueue->add($update);
    }
    $count = count($updates);
    $processed = count($items);

    $context['sandbox']['progress'] += $processed;
    $context['results']['added'] = $context['sandbox']['progress'];
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    $percent = round($context['finished'] * 100) . '%';
    $context['message'] = t('[@percent] Sent @count items to the DY update queue: @progress of @total items processed.', [
      '@percent' => $percent,
      '@progress' => $context['sandbox']['progress'],
      '@count' => $count,
      '@total' => $context['sandbox']['max'],
    ]);
  }

  /**
   * Batch finish callback.
   *
   * @param bool $success
   *   Did the batch succeed in all operations.
   * @param array $results
   *   Result data.
   * @param array $operations
   *   Any remaining operations.
   */
  public static function finish(bool $success, array $results, array $operations): void {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addStatus(t('Successfully processed @updated items.', [
        '@updated' => $results['added'],
      ]));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      $messenger->addError($message);
    }
  }

}
