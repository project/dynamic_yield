<?php

namespace Drupal\dynamic_yield;

/**
 * Maps known incompatibilities between Drupal and DY language codes.
 */
enum LanguageCodes: string {

  case ZhHans = 'zh-hans';
  case ZhHant = 'zh-hant';

  /**
   * Get the replacement string for a language.
   *
   * @return string
   *   The dynamic yield code.
   */
  public function getSubstitute(): string {
    return match ($this) {
      LanguageCodes::ZhHans => 'zh_CN',
      LanguageCodes::ZhHant => 'zh_HK',
    };
  }

}
