<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\key\Entity\Key;

/**
 * Defines the dynamic yield section entity type.
 *
 * @ConfigEntityType(
 *   id = "dy_section",
 *   label = @Translation("Dynamic Yield Section"),
 *   label_collection = @Translation("Dynamic Yield Sections"),
 *   label_singular = @Translation("Dynamic Yield Section"),
 *   label_plural = @Translation("Dynamic Yield Sections"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Dynamic Yield Section",
 *     plural = "@count Dynamic Yield Sections",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\dynamic_yield\Entity\DynamicYieldSectionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\dynamic_yield\Form\DySectionForm",
 *       "edit" = "Drupal\dynamic_yield\Form\DySectionForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "dy_section",
 *   admin_permission = "administer dy_section",
 *   links = {
 *     "collection" = "/admin/config/services/dynamic-yield/section",
 *     "add-form" = "/admin/config/services/dynamic-yield/section/add",
 *     "edit-form" = "/admin/config/services/dynamic-yield/section/{dy_section}/edit",
 *     "delete-form" = "/admin/config/services/dynamic-yield/section/{dy_section}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "section_id",
 *     "feed_id",
 *     "key_id",
 *   },
 * )
 */
final class DynamicYieldSection extends ConfigEntityBase {

  /**
   * The section machine name.
   */
  protected string $id;

  /**
   * The section label.
   */
  protected string $label;

  /**
   * The section description.
   */
  protected string $description;

  /**
   * The section identifier from Dynamic Yield.
   */
  protected string $section_id;

  /**
   * The section feed identifier from Dynamic Yield.
   */
  protected string $feed_id;

  /**
   * The identifier for the Key entity that holds the API key.
   */
  protected string $key_id;

  /**
   * Get the description.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string {
    return $this->description ?? '';
  }

  /**
   * Get the feed identifier.
   *
   * @return string
   *   The feed identifier.
   */
  public function getFeedId(): string {
    return $this->feed_id ?? '';
  }

  /**
   * Get the key identifier.
   *
   * @return string
   *   The key identifier.
   */
  public function getKeyId(): string {
    return $this->key_id ?? '';
  }

  /**
   * Get the Section ID used in dynamic yield.
   *
   * @return string
   *   The section identifier.
   */
  public function getSectionId(): string {
    return $this->section_id ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    /** @var \Drupal\key\KeyRepositoryInterface $keyRepository */
    $keyRepository = \Drupal::service('key.repository');
    $keyEntity = $keyRepository->getKey($this->getKeyId());
    if ($keyEntity instanceof Key) {
      $this->addDependency('config', $keyEntity->getConfigDependencyName());
    }
    return $this;
  }

}
