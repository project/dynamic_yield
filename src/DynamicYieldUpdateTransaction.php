<?php

namespace Drupal\dynamic_yield;

/**
 * A data object to hold the transaction id and items sent in an update.
 */
class DynamicYieldUpdateTransaction {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    private readonly string $transaction,
    private readonly int $timestamp,
    private array $updateItems,
  ) {}

  /**
   * Get the transaction ID.
   *
   * @return string
   *   The ID.
   */
  public function getTransaction(): string {
    return $this->transaction ?? '';
  }

  /**
   * Get all the remaining update items.
   *
   * @return \Drupal\dynamic_yield\DynamicYieldUpdateItem[]
   *   An array of DynamicYieldUpdateItem objects.
   */
  public function getUpdateItems(): array {
    return $this->updateItems ?? [];
  }

  /**
   * Get the number of remaining items.
   *
   * @return int
   *   The current item count.
   */
  public function countUpdateItems(): int {
    return count($this->getUpdateItems());
  }

  /**
   * Remove a single item from the record.
   *
   * @param string $id
   *   The item id to remove.
   */
  public function removeUpdateItem(string $id): void {
    unset($this->updateItems[$id]);
  }

  /**
   * Checks the timestamp vs. current time.
   *
   * @return bool
   *   Elapsed time exceeds 90 seconds.
   */
  public function expired(): bool {
    return time() - $this->timestamp > 90;
  }

  /**
   * Checks for a completely empty transaction.
   *
   * @return bool
   *   The transaction holds no meaningful data.
   */
  public function skipped(): bool {
    return $this->transaction === '' && $this->timestamp === 0 && $this->updateItems === [];
  }

}
