<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\dynamic_yield\DynamicYieldBatchSendUpdates;
use Drupal\dynamic_yield\Services\DynamicYieldUpdateCollectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Dynamic Yield form.
 */
final class FeedUpdateForm extends FormBase {

  /**
   * Injected priority queue.
   */
  private DynamicYieldUpdateCollectionInterface $dynamicYieldUpdates;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->dynamicYieldUpdates = $container->get('dynamic_yield.updates');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dynamic_yield_feed_update';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['introduction'] = [
      '#type' => 'item',
      '#title' => $this->t('Initiate a feed update'),
      '#markup' => $this->t('There are currently @count items waiting to be sent.', ['@count' => $this->dynamicYieldUpdates->count()]),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Update'),
        '#disabled' => $this->dynamicYieldUpdates->isEmpty(),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $batch = new BatchBuilder();
    $batch->setTitle($this->t('Updating feed'))
      ->setFinishCallback([DynamicYieldBatchSendUpdates::class, 'finish'])
      ->setInitMessage($this->t('Setting up...'))
      ->setProgressMessage($this->t('Processing...'))
      ->setErrorMessage($this->t('An error occurred during processing.'))
      ->addOperation([DynamicYieldBatchSendUpdates::class, 'process']);

    batch_set($batch->toArray());

    // Set the redirect for the form submission back to the form itself.
    $form_state->setRedirectUrl(Url::fromRoute('dynamic_yield.feed_update'));
  }

}
