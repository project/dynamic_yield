<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Form;

use Drupal\Core\Asset\LibraryDiscoveryCollector;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Dynamic Yield settings for this site.
 */
final class FeedSettingsForm extends ConfigFormBase {

  /**
   * Injected service.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Injected library service for cache invalidation.
   */
  protected LibraryDiscoveryCollector $libraryDiscovery;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->libraryDiscovery = $container->get('library.discovery.collector');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dynamic_yield_feed_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['dynamic_yield.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $sections = $this->buildSectionOptions();
    $form['section'] = [
      '#title' => $this->t('Dynamic Yield section'),
    ];
    if (empty($sections)) {
      $form['section']['#type'] = 'item';
      $form['section']['#markup'] = $this->t('There are no Dynamic Yield Sections yet.');
    }
    else {
      $form['section']['#type'] = 'radios';
      $form['section']['#description'] = $this->t('Select which Dynamic Yield section to use.');
      $form['section']['#options'] = $sections;
      $form['section']['#default_value'] = $this->config('dynamic_yield.settings')->get('section') ?? array_key_first($sections);
    }
    $form['content_type_introduction'] = [
      '#type' => 'item',
      '#title' => $this->t('Content Types'),
      '#markup' => $this->t('Select the content types below to include in the product feed.'),
    ];
    $defaultContentTypes = $this->config('dynamic_yield.settings')->get('content_types') ?? [];
    $form['content_types'] = [
      '#type' => 'tableselect',
      '#header' => ['type' => $this->t('Content type')],
      '#options' => $this->buildContentTypeOptions(),
      '#default_value' => $defaultContentTypes,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('dynamic_yield.settings')
      ->set('section', $form_state->getValue('section'))
      ->save();
    // Process content types.
    $contentTypes = [];
    foreach ($form_state->getValue('content_types') as $type => $checked) {
      $contentTypes[$type] = FALSE;
      if ($checked) {
        $contentTypes[$type] = TRUE;
      }
    }
    $this->config('dynamic_yield.settings')
      ->set('content_types', $contentTypes)
      ->save();

    // Library definitions depend on the selected section.
    $this->libraryDiscovery->clear();
    $this->messenger()->addStatus($this->t('Cached library definitions cleared.'));
  }

  /**
   * Build an option array for selecting the section.
   *
   * @return array
   *   An array of section options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildSectionOptions(): array {
    $options = [];
    $sections = $this->entityTypeManager->getStorage('dy_section')->loadMultiple();
    foreach ($sections as $section) {
      $options[$section->id()] = $section->label();
    }
    return $options;
  }

  /**
   * Build an option array.
   *
   * @return array
   *   An array of content type options.
   */
  protected function buildContentTypeOptions(): array {
    $options = [];
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $type) {
      $options[$type->id()] = ['type' => $type->label()];
    }
    return $options;
  }

}
