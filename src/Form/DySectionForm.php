<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Form;

use Drupal\Core\Asset\LibraryDiscoveryCollector;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dynamic_yield\Entity\DynamicYieldSection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Dynamic Yield Section form.
 */
final class DySectionForm extends EntityForm {

  /**
   * Document the type of this entity.
   *
   * @var \Drupal\dynamic_yield\Entity\DynamicYieldSection
   */
  protected $entity;

  /**
   * Injected library service for cache invalidation.
   */
  protected LibraryDiscoveryCollector $libraryDiscovery;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->libraryDiscovery = $container->get('library.discovery.collector');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [DynamicYieldSection::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->getDescription(),
    ];

    $form['section_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dynamic Yield Section identifier.'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getSectionId(),
      '#required' => TRUE,
    ];

    $form['feed_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product feed identifier.'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getFeedId(),
      '#required' => TRUE,
    ];

    $form['key_id'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Select a stored API Key'),
      '#default_value' => $this->entity->getKeyId(),
      '#empty_option' => $this->t('- Please select -'),
      '#key_filters' => ['type' => 'authentication'],
      '#description' => $this->t('Select the key you have configured to hold the API key.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    // Library definitions depend on section_id.
    $this->libraryDiscovery->clear();
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
        default => $this->t('Saved %label.', $message_args),
      }
    );
    $this->messenger()->addStatus($this->t('Cached library definitions cleared.'));
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
