<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Maintains the _dyid_server cookie.
 */
final class DynamicYieldCookieSubscriber implements EventSubscriberInterface {

  /**
   * Kernel response event handler.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    // Check that this is the master request.
    if (!$event->isMainRequest()) {
      return;
    }
    $request = $event->getRequest();
    $dyid = $request->cookies->get('_dyid');
    $dyidServer = $request->cookies->get('_dyid_server');
    if (!empty($dyid) && $dyid !== $dyidServer) {
      // A DY identifier cookie is present and either the server copy does not
      // exist or has a different value.
      $response = $event->getResponse();
      // Cookie expires in 1 year:
      $year = new \DateTime('+1 year');
      $cookie = Cookie::create(
        name: '_dyid_server',
        value: $dyid,
        expire: $year,
        httpOnly: FALSE);
      $response->headers->setCookie($cookie);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse'],
    ];
  }

}
