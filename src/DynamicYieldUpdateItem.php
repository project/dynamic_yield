<?php

namespace Drupal\dynamic_yield;

/**
 * This data object holds the data about a single update in the collection.
 */
class DynamicYieldUpdateItem {

  /**
   * This data object uses the following values.
   *
   * @param string $uuid
   *   The UUID of the entity to be updated.
   * @param string $entityTypeId
   *   The entity type of the entity to be updated.
   * @param int $timestamp
   *   The timestamp of when the entity was added to the collection.
   * @param \Drupal\dynamic_yield\FeedApiActions $action
   *   The action for this update.
   */
  public function __construct(
    public readonly string $uuid,
    public readonly string $entityTypeId,
    public readonly int $timestamp,
    public readonly FeedApiActions $action,
  ) {}

}
