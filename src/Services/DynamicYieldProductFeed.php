<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Utility\Error;
use Drupal\dynamic_yield\DynamicYieldUpdateTransaction;
use Drupal\dynamic_yield\Entity\DynamicYieldSection;
use Drupal\dynamic_yield\FeedApiActions;
use Drupal\key\Entity\Key;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Provides the integration between drupal content and DY data feed.
 *
 * @see https://dy.dev/reference/update-product-feed
 * @see https://dy.dev/reference/track-transaction-status-whole-transaction
 */
final class DynamicYieldProductFeed implements DynamicYieldProductFeedInterface {

  const string DY_API_BASE_URL = 'https://dy-api.com';
  const string BULK_UPDATE_PATH = '/v2/feeds/%s/bulk';

  const string TRANSACTION_STATUS_PATH = '/v2/feeds/%s/transaction/%s';

  /**
   * Constructs a DynamicYieldProductFeed object.
   */
  public function __construct(
    private readonly DynamicYieldUpdateCollectionInterface $dynamicYieldUpdates,
    private readonly DynamicYieldColumnPluginManager $dynamicYieldColumns,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly LoggerChannelInterface $loggerChannel,
    private readonly ClientFactory $clientFactory,
    private readonly KeyRepositoryInterface $keyRepository,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function process(int $count): DynamicYieldUpdateTransaction {
    // Copy items successfully loaded and processed into a new array.
    $processed = [];
    // Prepare the array to hold transmission data.
    $data = [
      'requests' => [],
    ];
    if ($this->dynamicYieldUpdates->isEmpty()) {
      return new DynamicYieldUpdateTransaction('', 0, []);
    }
    $items = $this->dynamicYieldUpdates->get($count);
    foreach ($items as $item) {
      $request = [];
      $request['id'] = $item->uuid;
      $request['action'] = $item->action->value;
      if ($item->action === FeedApiActions::Update) {
        // Load entity.
        $result = $this->entityTypeManager->getStorage($item->entityTypeId)->loadByProperties(['uuid' => $item->uuid]);
        $entity = array_shift($result);
        if (!($entity instanceof ContentEntityInterface)) {
          $this->loggerChannel->error('Entity type @type with uuid @uuid does not exist', [
            '@type' => $item->entityTypeId,
            '@uuid' => $item->uuid,
          ]);
          continue;
        }
        $request['body']['data'] = $this->getEntityData($entity);
      }
      $data['requests'][] = $request;
      $processed[$item->uuid] = $item;
    }
    $transactionID = $this->send($data);
    return new DynamicYieldUpdateTransaction($transactionID, time(), $processed);
  }

  /**
   * {@inheritdoc}
   */
  protected function send(array $data): string {
    $section = $this->getSection();
    $key = $this->getKey($section);
    $path = sprintf(self::BULK_UPDATE_PATH, $section->getFeedId());
    $options = [
      'base_uri' => self::DY_API_BASE_URL,
      'body' => json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
      'headers' => [
        'accept' => 'application/json',
        'content-type' => 'application/json',
        'dy-api-key' => $key->getKeyValue(),
      ],
    ];
    $client = $this->clientFactory->fromOptions($options);
    try {
      $response = $client->post($path);
      if ($response->getStatusCode() === 202) {
        $responseData = json_decode($response->getBody()->getContents(), TRUE);
        if ($responseData['success']) {
          return $responseData['data']['transaction_id'];
        }
      }
      // Log any non-exception errors.
      $this->loggerChannel->error('Failed to send dynamic yield feed with http code @code.', ['@code' => $response->getStatusCode()]);
    }
    catch (GuzzleException $e) {
      Error::logException($this->loggerChannel, $e);
    }
    // Request failed - there is no transaction ID.
    return '';
  }

  /**
   * Verifies the status of a Dynamic Yield update transaction.
   *
   * @param \Drupal\dynamic_yield\DynamicYieldUpdateTransaction $transaction
   *   The transaction to verify.
   */
  public function verify(DynamicYieldUpdateTransaction $transaction): void {
    $id = $transaction->getTransaction();
    if ($id === '') {
      // Nothing to do if the transaction id is empty.
      return;
    }
    $section = $this->getSection();
    $key = $this->getKey($section);
    $delay = 0;
    $path = sprintf(self::TRANSACTION_STATUS_PATH, $section->getFeedId(), $transaction->getTransaction());
    $options = [
      'base_uri' => self::DY_API_BASE_URL,
      'headers' => [
        'accept' => 'application/json',
        'dy-api-key' => $key->getKeyValue(),
      ],
    ];
    $client = $this->clientFactory->fromOptions($options);
    do {
      try {
        $exit = FALSE;
        // Add a delay if needed.
        $addDelay = $delay > 0 ? ['delay' => $delay] : [];
        $response = $client->get($path, $addDelay);
        if ($response->getStatusCode() === 200) {
          // If we have a successful request, move to true.
          $exit = TRUE;
          $json = $response->getBody()->getContents();
          if (empty($json)) {
            $exit = FALSE;
            $this->loggerChannel->warning("Transaction check for $id returned an empty response.");
            continue;
          }

          $statuses = json_decode($json, TRUE);
          if (empty($statuses)) {
            $exit = FALSE;
            $this->loggerChannel->warning("Transaction check for $id returned an empty status set.");
            continue;
          }
          foreach ($statuses as $status) {
            // At least one possible status besides success is `pending`.
            $updated = $status['status'] === 'success';
            if ($updated) {
              $transaction->removeUpdateItem($status['item']);
            }
            // If all items succeed, $exit will stay TRUE.
            $exit = $exit && $updated;
          }
        }
      }
      catch (ClientException $e) {
        $response = $e->getResponse();
        if ($response->getStatusCode() === 429) {
          // Too many requests. Increase delay.
          // Max rate in DY is 600 requests/60 sec.
          // Which is 1 request/0.1 seconds.
          // 0.1 seconds = 100 milliseconds.
          $delay = $delay + 100;
          $exit = FALSE;
        }
        else {
          Error::logException($this->loggerChannel, $e);
          // Any other code, log and exit.
          $exit = TRUE;
        }
      }
      catch (\Exception $e) {
        // Log and exit.
        Error::logException($this->loggerChannel, $e);
        $exit = TRUE;
      }
      if ($transaction->expired() && !$exit) {
        $this->loggerChannel->error("Transaction $id expired, aborting.");
      }
    } while (!$exit && !$transaction->expired());
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityData(ContentEntityInterface $entity): array {
    $data = [];
    foreach ($this->dynamicYieldColumns->getColumns() as $name => $column) {
      $value = $column->process($entity);
      $data[$name] = $value;
    }
    return $data;
  }

  /**
   * Retrieves the Dynamic Yield section.
   *
   * @return \Drupal\dynamic_yield\Entity\DynamicYieldSection
   *   The Dynamic Yield section entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getSection(): DynamicYieldSection {
    $sectionId = $this->configFactory->get('dynamic_yield.settings')
      ->get('section');
    $section = $this->entityTypeManager->getStorage('dy_section')
      ->load($sectionId);
    if (!($section instanceof DynamicYieldSection)) {
      $this->loggerChannel->error('Section @section set in Dynamic Yield configuration does not exist', ['@section' => $sectionId]);
      throw new \UnexpectedValueException("Section $sectionId set in Dynamic Yield configuration returns null from storage");
    }
    return $section;
  }

  /**
   * Load the Key entity holding the api key value.
   *
   * @param \Drupal\dynamic_yield\Entity\DynamicYieldSection $section
   *   The active section.
   *
   * @return \Drupal\key\Entity\Key
   *   The key set in the section entity.
   */
  protected function getKey(DynamicYieldSection $section): Key {
    $key = $this->keyRepository->getKey($section->getKeyId());
    if (!($key instanceof Key)) {
      $this->loggerChannel->error('Key @key set in Dynamic Yield Section @section does not exist.', [
        '@key' => $section->getKeyId(),
        '@section' => $section->id(),
      ]);
      throw new \UnexpectedValueException("Key not found in repository.");
    }
    return $key;
  }

}
