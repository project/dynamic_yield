<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Services;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\dynamic_yield\DynamicYieldUpdateItem;
use Drupal\dynamic_yield\FeedApiActions;

/**
 * A factory for creating DynamicYieldUpdateItem from filtered entities.
 */
interface UpdateFactoryInterface {

  /**
   * Applies filters to a single entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to evaluate.
   * @param \Drupal\dynamic_yield\FeedApiActions $action
   *   The action to use in the update item.
   *
   * @return \Drupal\dynamic_yield\DynamicYieldUpdateItem|null
   *   Returns a DynamicYieldUpdateItem if it passes all filters.
   */
  public function filter(ContentEntityInterface $entity, FeedApiActions $action): ?DynamicYieldUpdateItem;

  /**
   * Applies filters to a set of entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   A set of entities to filter.
   *
   * @return \Drupal\dynamic_yield\DynamicYieldUpdateItem[]
   *   DynamicYieldUpdateItems for all entities that pass.
   */
  public function filterMultiple(array $entities): array;

}
