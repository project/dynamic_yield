<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Services;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\dynamic_yield\DynamicYieldUpdateTransaction;

/**
 * Describes the integration between drupal content and DY data feed.
 *
 * @see https://dy.dev/reference/update-product-feed
 * @see https://dy.dev/reference/track-transaction-status-whole-transaction
 */
interface DynamicYieldProductFeedInterface {

  /**
   * Prepare and send a set of updates to Dynamic Yield.
   *
   * @param int $count
   *   The number of items to load and send.
   *
   * @see https://dy.dev/reference/update-product-feed
   *
   * @return \Drupal\dynamic_yield\DynamicYieldUpdateTransaction
   *   Data needed to verify the push.
   */
  public function process(int $count): DynamicYieldUpdateTransaction;

  /**
   * Verifies that the transaction tracked has succeeded.
   *
   * Any unprocessed items are processed and sent at a later time.
   *
   * @param \Drupal\dynamic_yield\DynamicYieldUpdateTransaction $transaction
   *   The transaction data object.
   */
  public function verify(DynamicYieldUpdateTransaction $transaction): void;

  /**
   * Get the Dynamic Yield column data for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The source entity.
   *
   * @return array<string, bool|float|string>
   *   The processed data indexed by column name.
   */
  public function getEntityData(ContentEntityInterface $entity): array;

}
