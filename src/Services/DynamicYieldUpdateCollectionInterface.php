<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Services;

use Drupal\dynamic_yield\DynamicYieldUpdateItem;

/**
 * Maintains a priority queue of DynamicYieldUpdateItem objects.
 */
interface DynamicYieldUpdateCollectionInterface extends \Countable {

  /**
   * Adds an entity's data to the collection.
   *
   * @param \Drupal\dynamic_yield\DynamicYieldUpdateItem $item
   *   The item to be added to the collection.
   */
  public function add(DynamicYieldUpdateItem $item): void;

  /**
   * Removes multiple items from the collection.
   *
   * @param \Drupal\dynamic_yield\DynamicYieldUpdateItem[] $items
   *   A set items to be removed.
   */
  public function remove(array $items): void;

  /**
   * Returns up to a specified number of items.
   *
   * @param int $limit
   *   The maximum number of items to return.
   *
   * @return \Drupal\dynamic_yield\DynamicYieldUpdateItem[]
   *   An array of update items.
   */
  public function get(int $limit): array;

  /**
   * Check if the collection is empty.
   *
   * @return bool
   *   True if no items are in the collection.
   */
  public function isEmpty(): bool;

}
