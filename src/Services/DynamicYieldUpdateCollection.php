<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Services;

use Drupal\Core\Database\Connection;
use Drupal\dynamic_yield\DynamicYieldUpdateItem;
use Drupal\dynamic_yield\FeedApiActions;

/**
 * This class maintains a priority queue of entities that need to be updated.
 *
 * The priority queue is stored in a custom database table and is ordered by
 * the timestamp value.
 *
 * @see https://en.wikipedia.org/wiki/Priority_queue
 */
class DynamicYieldUpdateCollection implements DynamicYieldUpdateCollectionInterface {

  const string DATA_TABLE_NAME = 'dynamic_yield_update';

  /**
   * Constructs a DynamicYieldUpdateCollection object.
   */
  public function __construct(
    private readonly Connection $database,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function add(DynamicYieldUpdateItem $item): void {
    // If the entity is already waiting for update,
    // leave it at its current priority.
    $exists = $this->database->select(self::DATA_TABLE_NAME, 'dy')
      ->fields('dy', ['uuid'])
      ->condition('uuid', $item->uuid)
      ->countQuery()
      ->execute()
      ->fetchField();
    if ($exists) {
      return;
    }
    $this->database->insert(self::DATA_TABLE_NAME)
      ->fields(['uuid', 'entity_type', 'timestamp', 'action'])
      ->values(
        [
          'uuid' => $item->uuid,
          'entity_type' => $item->entityTypeId,
          'timestamp' => $item->timestamp,
          'action' => $item->action->value,
        ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function remove(array $items): void {
    $uuids = array_map(
      function (DynamicYieldUpdateItem $item) {
        return $item->uuid;
      },
      $items
    );
    $this->database->delete(self::DATA_TABLE_NAME)
      ->condition('uuid', $uuids, 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function get(int $limit): array {
    // Get the items from the database.
    $items = $this->database->select(self::DATA_TABLE_NAME, 'dy')
      ->fields('dy')
      ->orderBy('timestamp')
      ->range(0, $limit)
      ->execute()
      ->fetchAll();
    $items = array_map(
      function (\StdClass $item) {
        return new DynamicYieldUpdateItem($item->uuid, $item->entity_type, (int) $item->timestamp, FeedApiActions::from($item->action));
      },
      $items
    );
    // Found items are removed from the priority queue and returned.
    $this->remove($items);
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    // PHP PDO returns all values as strings.
    return (int) $this->database->select(self::DATA_TABLE_NAME, 'dy')
      ->fields('dy')
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return $this->count() === 0;
  }

}
