<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\dynamic_yield\Attribute\DynamicYieldColumn;
use Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface;

/**
 * DynamicYieldColumn plugin manager.
 */
final class DynamicYieldColumnPluginManager extends DefaultPluginManager {

  /**
   * The column plugins indexed by name.
   *
   * @var \Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface[]
   */
  protected array $columns = [];

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/DynamicYieldColumn', $namespaces, $module_handler, DynamicYieldColumnInterface::class, DynamicYieldColumn::class);
    $this->alterInfo('dynamic_yield_column_info');
    $this->setCacheBackend($cache_backend, 'dynamic_yield_column_plugins');
  }

  /**
   * Get all the column plugins as an array indexed by column name.
   *
   * @return \Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface[]
   *   The plugins.
   */
  public function getColumns(): array {
    if (!empty($this->columns)) {
      return $this->columns;
    }
    $definitions = $this->getDefinitions();
    foreach ($definitions as $definition) {
      $this->columns[$definition['name']] = $this->createInstance($definition['id']);
    }
    return $this->columns;
  }

}
