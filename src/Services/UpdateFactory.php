<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Services;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\dynamic_yield\DynamicYieldUpdateItem;
use Drupal\dynamic_yield\FeedApiActions;

/**
 * Creates DynamicYieldUpdateItem instances from entities.
 */
final class UpdateFactory implements UpdateFactoryInterface {

  /**
   * Constructs an UpdateFactory object.
   */
  public function __construct(
    private readonly DynamicYieldFilterPluginManager $filterManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function filter(ContentEntityInterface $entity, FeedApiActions $action): ?DynamicYieldUpdateItem {
    $filters = $this->filterManager->getFilters();
    $pass = TRUE;
    foreach ($filters as $filter) {
      $pass = $pass && $filter->isIncluded($entity, $action);
    }
    if ($pass) {
      return new DynamicYieldUpdateItem(
        uuid: $entity->uuid(),
        entityTypeId: $entity->getEntityTypeId(),
        timestamp: time(),
        action: $action
      );
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function filterMultiple(array $entities, FeedApiActions $action = FeedApiActions::Update): array {
    $updates = [];
    $filters = $this->filterManager->getFilters();
    foreach ($entities as $entity) {
      $pass = TRUE;
      foreach ($filters as $filter) {
        $pass = $pass && $filter->isIncluded($entity, $action);
      }
      if ($pass) {
        $updates[] = new DynamicYieldUpdateItem(
          uuid: $entity->uuid(),
          entityTypeId: $entity->getEntityTypeId(),
          timestamp: time(),
          action: $action
        );
      }
    }
    return $updates;
  }

}
