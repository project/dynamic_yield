<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\dynamic_yield\Attribute\DynamicYieldFilter;
use Drupal\dynamic_yield\Plugin\DynamicYieldFilterInterface;

/**
 * DynamicYieldFilter plugin manager.
 */
final class DynamicYieldFilterPluginManager extends DefaultPluginManager {

  /**
   * The column plugins indexed by name.
   *
   * @var \Drupal\dynamic_yield\Plugin\DynamicYieldFilterInterface[]
   */
  protected array $filters = [];

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/DynamicYieldFilter', $namespaces, $module_handler, DynamicYieldFilterInterface::class, DynamicYieldFilter::class);
    $this->alterInfo('dynamic_yield_filter_info');
    $this->setCacheBackend($cache_backend, 'dynamic_yield_filter_plugins');
  }

  /**
   * Get all the column plugins as an array indexed by column name.
   *
   * @return \Drupal\dynamic_yield\Plugin\DynamicYieldFilterInterface[]
   *   The plugins.
   */
  public function getFilters(): array {
    if (!empty($this->filters)) {
      return $this->filters;
    }
    $definitions = $this->getDefinitions();
    foreach ($definitions as $definition) {
      $this->filters[] = $this->createInstance($definition['id']);
    }
    return $this->filters;
  }

}
