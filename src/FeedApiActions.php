<?php

namespace Drupal\dynamic_yield;

/**
 * Valid action verbs for Dynamic Yield product feeds.
 */
enum FeedApiActions: string {
  case Delete = 'delete';
  case Update = 'update';
}
