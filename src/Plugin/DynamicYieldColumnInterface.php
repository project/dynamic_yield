<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Plugin;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for dynamic_yield_column plugins.
 */
interface DynamicYieldColumnInterface {

  /**
   * Returns the calculated column value.
   */
  public function process(ContentEntityInterface $entity): string|bool|float;

}
