<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Plugin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\dynamic_yield\FeedApiActions;

/**
 * DynamicYieldFilter plugins determine entities included in the feed.
 */
interface DynamicYieldFilterInterface {

  /**
   * Applies filter logic to determine if the entity should be included.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to evaluate.
   * @param \Drupal\dynamic_yield\FeedApiActions $action
   *   The action proposed for this entity.
   *
   * @return bool
   *   FALSE if the entity should be excluded
   */
  public function isIncluded(ContentEntityInterface $entity, FeedApiActions $action): bool;

}
