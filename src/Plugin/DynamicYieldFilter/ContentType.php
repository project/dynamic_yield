<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Plugin\DynamicYieldFilter;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\dynamic_yield\Attribute\DynamicYieldFilter;
use Drupal\dynamic_yield\FeedApiActions;
use Drupal\dynamic_yield\Plugin\DynamicYieldFilterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the dynamic_yield_filter.
 */
#[DynamicYieldFilter(
  id: 'content_type',
  label: new TranslatableMarkup('Content type'),
  description: new TranslatableMarkup('Allows content types enabled in dynamic_yield.settings'),
)]
class ContentType extends PluginBase implements DynamicYieldFilterInterface, ContainerFactoryPluginInterface {

  /**
   * Injected configuration service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function isIncluded(ContentEntityInterface $entity, FeedApiActions $action): bool {
    $config = $this->configFactory->get('dynamic_yield.settings');
    $settings = $config->get('content_types');
    // Reduce the array to enabled bundles.
    $enabled = array_filter($settings, function ($setting) {
      return $setting;
    });
    // Bundle ids are keys.
    $bundles = array_keys($enabled);
    return in_array($entity->bundle(), $bundles);
  }

}
