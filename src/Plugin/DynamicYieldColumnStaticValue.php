<?php

namespace Drupal\dynamic_yield\Plugin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for dynamic_yield_column plugins that return a fixed value.
 *
 * The sourceField property of the plugin will be used to define the return
 * value.  The value of sourceField should be the prefix 'type:' followed
 * by the value, where type is one of {string, bool, float}.
 *
 * For example, a plugin with the attribute configured as:
 *   sourceField: 'bool:0',
 * will always return FALSE as the processed value.
 */
abstract class DynamicYieldColumnStaticValue extends PluginBase implements DynamicYieldColumnInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity): string|bool|float {
    $source = explode(':', $this->pluginDefinition['sourceField']);
    [$type, $value] = $source;
    return match ($type) {
      'string' => $value,
      'bool' => (bool) $value,
      'float' => (float) $value,
      default => throw new \UnhandledMatchError($type),
    };
  }

}
