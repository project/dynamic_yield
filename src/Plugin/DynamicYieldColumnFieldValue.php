<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Base class for dynamic_yield_column plugins that simply use a field value.
 *
 * The sourceField property of the plugin will be used as a field name on the
 * source entity.
 */
abstract class DynamicYieldColumnFieldValue extends PluginBase implements DynamicYieldColumnInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity): string {
    $value = '';
    if (!is_null($this->pluginDefinition['sourceField']) && $entity->hasField($this->pluginDefinition['sourceField'])) {
      $value = $entity->get($this->pluginDefinition['sourceField'])->getValue();
      if (is_array($value)) {
        if ($value === []) {
          $value = '';
        }
        $value = array_shift($value);
      }
      if (is_bool($value)) {
        $value = $value ? 'true' : 'false';
      }
    }
    return (string) $value;
  }

}
