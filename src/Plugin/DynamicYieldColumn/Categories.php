<?php

namespace Drupal\dynamic_yield\Plugin\DynamicYieldColumn;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\dynamic_yield\Attribute\DynamicYieldColumn;
use Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface;

/**
 * Plugin implementation for the categories column.
 */
#[DynamicYieldColumn(
  id: 'categories',
  name: 'categories',
  description: new TranslatableMarkup('Categories is required by Dynamic Yield'),
)]
class Categories implements DynamicYieldColumnInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity): string {
    $path = $entity->toUrl('canonical')->toString();
    if ($path === '/') {
      return 'home';
    }
    $path = ltrim($path, '/');
    // We want to drop the last component of the path as that component is about
    // the content item and not its context.
    // Except we do not want drop the component if it's the only component.
    $segments = explode('/', $path);
    if (count($segments) > 1) {
      array_pop($segments);
    }
    return implode('|', $segments);
  }

}
