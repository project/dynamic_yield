<?php

namespace Drupal\dynamic_yield\Plugin\DynamicYieldColumn;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\dynamic_yield\Attribute\DynamicYieldColumn;
use Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface;

/**
 * Plugin implementation for the name column.
 */
#[DynamicYieldColumn(
  id: 'url',
  name: 'url',
  description: new TranslatableMarkup('Url is required by Dynamic Yield'),
)]
class Url implements DynamicYieldColumnInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity): string {
    return $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
  }

}
