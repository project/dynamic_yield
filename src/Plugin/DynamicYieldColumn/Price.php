<?php

namespace Drupal\dynamic_yield\Plugin\DynamicYieldColumn;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\dynamic_yield\Attribute\DynamicYieldColumn;
use Drupal\dynamic_yield\Plugin\DynamicYieldColumnStaticValue;

/**
 * Plugin implementation for the price column.
 */
#[DynamicYieldColumn(
  id: 'price',
  name: 'price',
  sourceField: 'float:0.00',
  description: new TranslatableMarkup('Price is required by Dynamic Yield'),
)]
class Price extends DynamicYieldColumnStaticValue {}
