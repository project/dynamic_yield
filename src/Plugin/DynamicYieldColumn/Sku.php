<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Plugin\DynamicYieldColumn;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\dynamic_yield\Attribute\DynamicYieldColumn;
use Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface;

/**
 * Plugin implementation for the sku column.
 */
#[DynamicYieldColumn(
  id: 'sku',
  name: 'sku',
  description: new TranslatableMarkup('Unique identifier for Dynamic Yield'),
)]
final class Sku implements DynamicYieldColumnInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity): string {
    return $entity->uuid();
  }

}
