<?php

namespace Drupal\dynamic_yield\Plugin\DynamicYieldColumn;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\dynamic_yield\Attribute\DynamicYieldColumn;
use Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface;

/**
 * Plugin implementation for the in_stock column.
 */
#[DynamicYieldColumn(
  id: 'in_stock',
  name: 'in_stock',
  description: new TranslatableMarkup('"In Stock" is required by Dynamic Yield'),
)]
class InStock implements DynamicYieldColumnInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity): bool {
    if ($entity instanceof EntityPublishedInterface) {
      return $entity->isPublished();
    }
    return TRUE;
  }

}
