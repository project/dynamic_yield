<?php

namespace Drupal\dynamic_yield\Plugin\DynamicYieldColumn;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\dynamic_yield\Attribute\DynamicYieldColumn;
use Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for the keywords column.
 */
#[DynamicYieldColumn(
  id: 'keywords',
  name: 'keywords',
  description: new TranslatableMarkup('Keywords is recommended by Dynamic Yield'),
)]
class Keywords implements DynamicYieldColumnInterface, ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * A simple indexed array of vocabulary IDs.
   *
   * @var string[]
   */
  protected array $excludedVocabularies = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity): string {
    $keywords = '';
    if ($entity instanceof NodeInterface) {
      $names = $this->getNodeTermNames($entity);
      $keywords = implode('|', $names);
    }
    // @todo implement a general helper method for other entity types.
    return $keywords;
  }

  /**
   * Get all the term names of taxonomy terms associated a node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node being processed.
   *
   * @return string[]
   *   An array of term names associated with the node.
   */
  protected function getNodeTermNames(NodeInterface $node): array {
    $query = $this->database->select('taxonomy_index', 'ti')
      ->condition('nid', $node->id());
    $query->join('taxonomy_term_field_data', 'tf', 'tf.tid = ti.tid');
    if (!empty($this->excludedVocabularies)) {
      $query->condition('tf.vid', $this->excludedVocabularies, 'NOT IN');
    }
    return $query->fields('tf', ['name'])
      ->execute()
      ->fetchCol();
  }

}
