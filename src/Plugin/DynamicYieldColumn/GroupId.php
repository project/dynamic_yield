<?php

declare(strict_types=1);

namespace Drupal\dynamic_yield\Plugin\DynamicYieldColumn;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\dynamic_yield\Attribute\DynamicYieldColumn;
use Drupal\dynamic_yield\Plugin\DynamicYieldColumnInterface;

/**
 * Plugin implementation for the group_id column.
 */
#[DynamicYieldColumn(
  id: 'group_id',
  name: 'group_id',
  description: new TranslatableMarkup('Group ID for Dynamic Yield also uses entity uuid'),
)]
final class GroupId implements DynamicYieldColumnInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity): string {
    return $entity->uuid();
  }

}
