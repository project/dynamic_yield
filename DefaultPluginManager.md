classDiagram
direction BT
class CacheableDependencyInterface {
   getCacheTags() 
   getCacheMaxAge() 
   getCacheContexts() 
}
class CachedDiscoveryInterface {
   useCaches(use_caches) 
   clearCachedDefinitions() 
}
class DefaultPluginManager {
    pluginDefinitionAttributeName
    pluginDefinitionAnnotationName
    pluginInterface
    additionalAnnotationNamespaces
    moduleHandler
    subdir
    cacheKey
    defaults
    cacheTags
    alterHook
    moduleExtensionList
    namespaces
   getCachedDefinitions() 
   getCacheTags() 
   providerExists(provider) 
   useCaches(use_caches) 
   getDefinitions() 
   clearCachedDefinitions() 
   extractProviderFromDefinition(plugin_definition) 
   processDefinition(definition, plugin_id) 
   setCachedDefinitions(definitions) 
   getCacheMaxAge() 
   findDefinitions() 
   setCacheBackend(cache_backend, cache_key, cache_tags) 
   alterInfo(alter_hook) 
   getCacheContexts() 
   alterDefinitions(definitions) 
   getFactory() 
   getDiscovery() 
}
class DiscoveryCachedTrait {
    definitions
   getDefinition(plugin_id, exception_on_invalid) 
}
class DiscoveryInterface {
   getDefinitions() 
   getDefinition(plugin_id, exception_on_invalid) 
   hasDefinition(plugin_id) 
}
class DiscoveryTrait {
   doGetDefinition(definitions, plugin_id, exception_on_invalid) 
   getDefinition(plugin_id, exception_on_invalid) 
   getDefinitions() 
   hasDefinition(plugin_id) 
}
class DynamicYieldColumnInterface {
   process(entity) 
}
class DynamicYieldColumnPluginManager {
    columns
   getColumns() 
}
class FactoryInterface {
   createInstance(plugin_id, configuration) 
}
class MapperInterface {
   getInstance(options) 
}
class PluginManagerBase {
    factory
    discovery
    mapper
   getDiscovery() 
   handlePluginNotFound(plugin_id, configuration) 
   getInstance(options) 
   getFallbackPluginId(plugin_id, configuration) 
   getDefinitions() 
   getDefinition(plugin_id, exception_on_invalid) 
   createInstance(plugin_id, configuration) 
   getFactory() 
}
class PluginManagerInterface
class UseCacheBackendTrait {
    useCaches
    cacheBackend
   cacheSet(cid, data, expire, tags) 
   cacheGet(cid) 
}

CachedDiscoveryInterface  -->  DiscoveryInterface 
DefaultPluginManager  ..>  CacheableDependencyInterface 
DefaultPluginManager  ..>  CachedDiscoveryInterface 
DefaultPluginManager  ..>  DiscoveryCachedTrait 
DefaultPluginManager  -->  PluginManagerBase 
DefaultPluginManager  ..>  PluginManagerInterface 
DefaultPluginManager  ..>  UseCacheBackendTrait 
DiscoveryCachedTrait  ..>  DiscoveryTrait 
DynamicYieldColumnPluginManager  -->  DefaultPluginManager 
PluginManagerBase  ..>  DiscoveryTrait 
PluginManagerBase  ..>  PluginManagerInterface 
PluginManagerInterface  -->  DiscoveryInterface 
PluginManagerInterface  -->  FactoryInterface 
PluginManagerInterface  -->  MapperInterface 
