<?php

/**
 * @file
 * Install, update and uninstall functions for the Dynamic Yield module.
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\dynamic_yield\Services\DynamicYieldUpdateCollection;

/**
 * Implements hook_schema() for dynamic_yield.
 */
function dynamic_yield_schema(): array {
  $schema = [
    DynamicYieldUpdateCollection::DATA_TABLE_NAME => [
      'description' => 'Storage for the priority queue of updates maintained by the dynamic_yield module.',
      'fields' => [
        'uuid' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The UUID of the entity.',
        ],
        'entity_type' => [
          'type' => 'varchar_ascii',
          'length' => EntityTypeInterface::ID_MAX_LENGTH,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The type of the entity.',
        ],
        'timestamp' => [
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
          'description' => 'The unix timestamp that the row was created.',
        ],
        'action' => [
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The action type for this update item.',
        ],
      ],
      'primary key' => [
        'uuid',
      ],
      'indexes' => [
        'uuid' => ['uuid'],
        'timestamp' => ['timestamp'],
      ],
    ],
  ];
  return $schema;
}
