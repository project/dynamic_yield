## INTRODUCTION

The Dynamic Yield module provides an integration with the [Dynamic Yield](https://www.dynamicyield.com/)
personalization service.

## REQUIREMENTS

This module requires the [Key module](https://www.drupal.org/project/key) to
securely store the [Dynamic Yield api key](https://dy.dev/docs/product-feed-api#generate-an-api-key-to-include-in-each-api-call).

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Login to Dynamic Yield and create a [section](https://support.dynamicyield.com/hc/en-us/articles/360022833894-Managing-Sections).
- Note the section ID number.
- Create a product feed and set it to sync via API.
- Note the feed ID number.
- Create your API key and copy it into a secure storage location used by your
  Key module key.
- Create and configure sections in your Drupal site at
  `admin/config/services/dynamic-yield/section`.
- Create and configure the feed at
  `/admin/config/services/dynamic-yield/feed`

## DEVELOPMENT & CONTRIBUTION

This module is configured for [DDEV](https://ddev.readthedocs.io/en/stable/)
with the [Drupal contrib add-on](https://github.com/ddev/ddev-drupal-contrib)
for for easy local development & testing.


