/**
 * @file
 * Use `Drupal.behaviors` for cookie maintenance.
 */

(function (Drupal, once) {

  /**
   * Issue a request to the site with cache busting to trigger cookie tending.
   */
  function dynamicYieldServerCookie() {
    // An arrow function to check that a cookie is set by name.
    const cookieExists = (cookieName, cookieArray) =>
      cookieArray.some((item) => item.trim().startsWith(`${cookieName}=`));

    const cookieArray = document.cookie.split(";");

    const queryParameter = function () {
      const generated = new Uint32Array(1);
      const time = Date.now();
      window.crypto.getRandomValues(generated)
      return `${time}-${generated[0]}`;
    };

    if (
      cookieExists("_dyid", cookieArray) &&
      cookieExists("_dyid_server", cookieArray)
    ) {
      return;
    }

    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/dynamic-yield/xhr?dyduplicate=${queryParameter()}`);
    xhr.responseType = 'json';
    xhr.send();
  }

  /* Initialize listeners. */

  Drupal.behaviors.dyServer = {
    attach: () => {
      if (!once('dyServer', 'html').length) {
        // The array of elements without a dyServer id is empty.
        return;
      }
      window.addEventListener('load', dynamicYieldServerCookie);
    }
  };

})(Drupal, once);
