(function dyPageContext(drupalSettings) {
  ("use strict");
  window.DY = window.DY || {};

  DY.recommendationContext = drupalSettings.dynamicYield.context;

})(drupalSettings);
