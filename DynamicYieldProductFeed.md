classDiagram
direction BT
class DynamicYieldProductFeed {
    DY_API_BASE_URL
    BULK_UPDATE_PATH
    TRANSACTION_STATUS_PATH
    configFactory
    keyRepository
    loggerChannel
    entityTypeManager
    clientFactory
    dynamicYieldColumns
    dynamicYieldUpdates
   getSection() 
   send(data) 
   getKey(section) 
   verify(transaction) 
   getEntityData(entity) 
   process(count) 
}
class DynamicYieldProductFeedInterface {
   verify(transaction) 
   getEntityData(entity) 
   process(count) 
}

DynamicYieldProductFeed  ..>  DynamicYieldProductFeedInterface 
