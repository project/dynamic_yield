<?php

/**
 * @file
 * Post-update functions for the Dynamic Yield module.
 */

use Drupal\language\ConfigurableLanguageManagerInterface;

/**
 * Save any configurable languages to add any substitute language codes.
 */
function dynamic_yield_post_update_dy_init_languages() {
  // Resave configurable languages to trigger third party settings.
  $language_manager = \Drupal::languageManager();
  if ($language_manager instanceof ConfigurableLanguageManagerInterface) {
    // Multilingual.
    $languages = $language = \Drupal::entityTypeManager()
      ->getStorage('configurable_language')
      ->loadMultiple();
    foreach ($languages as $language) {
      $language->save();
    }
  }
}
