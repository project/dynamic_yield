classDiagram
direction BT
class Countable {
   count() 
}
class DynamicYieldUpdateCollection {
    DATA_TABLE_NAME
    database
   get(limit) 
   remove(items) 
   isEmpty() 
   add(item) 
   count() 
}
class DynamicYieldUpdateCollectionInterface {
   get(limit) 
   remove(items) 
   isEmpty() 
   add(item) 
}
class DynamicYieldUpdateItem {
    action
    entityTypeId
    uuid
    timestamp
}

DynamicYieldUpdateCollection  ..>  DynamicYieldUpdateCollectionInterface 
DynamicYieldUpdateCollectionInterface  -->  Countable 
